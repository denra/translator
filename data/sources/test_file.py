"""
this
is
docString
"""
from PyQt5.QtGui import QPixmap, QPainter, QTransform, QPalette
from PyQt5.QtWidgets import (QGraphicsScene, QGraphicsItem,
                             QGraphicsView, QLabel, QDialog,
                             QWidget, QInputDialog, QVBoxLayout, QFrame)

from PyQt5.QtCore import QTimer, Qt, QRectF, QSize, QCoreApplication
from gameLogic import Direction, directions, MapItem
import os.path
import random

MAP_ITEMS_PATHS = {"food.png": "0", "wall.png": "1", "empty.png": "2",
                   "gh1.png": "3", "gh2.png": "4", "gh3.png": "5",
                   "gh4.png": "6", "gh5.png": "7", "cherry.png": 'cherry',
                   "heart.png": 'heart', "shield.png": "shield"}

SPRITES_PATH = ["Blinky.png", "Inky.png", "Pinky.png", "Clyde.png",
                "Pacman.png", "PacmanDeath.png"]

ITEM_SIZE = QSize(32, 32)


class UnitAnimation:
    def __init__(self, file_name, size, label):
        self.frame_size = size
        self.current_frame = 0
        self.label = label
        self.current_direction = Direction.left
        self.sprites = self.unpack_sprites(GameField.SPRITES[file_name])
        self.timer = QTimer()
        self.timer.timeout.connect(self.next_frame)
        self.timer.start(70)

    def next_frame(self):
        self.current_frame += 1
        if self.current_frame >= len(self.sprites[self.current_direction]):
            self.current_frame = 0

        self.label.setPixmap(self.get_current_pixmap())

    def get_current_pixmap(self):
        return self.sprites[self.current_direction][self.current_frame]

    def unpack_sprites(self, input_pixmap):
        result = {Direction.right: [], Direction.left: [],
                  Direction.up: [], Direction.down: []}
        keys = [Direction.right, Direction.left, Direction.up, Direction.down]
        current_key = 0
        frame_count = input_pixmap.width() // (self.frame_size.width() * 4)

        for current_frame in range(frame_count * 4):
            frame = input_pixmap.copy(current_frame * self.frame_size.width(),
                                      0, self.frame_size.width(),
                                      self.frame_size.height())

            result[keys[current_key]].append(frame)
            if (current_frame + 1) % frame_count == 0:
                current_key += 1

        return result


class PacmanDeathAnimation:
    def __init__(self, file_name, label):
        self.current_frame = 0
        self.label = label
        self.sprites = self.unpack_sprites(GameField.SPRITES[file_name])

        self.timer = QTimer()
        self.timer.timeout.connect(self.next_frame)

    def next_frame(self):
        self.current_frame += 1
        if self.current_frame >= len(self.sprites) - 1:
            self.current_frame = 0
            self.timer.stop()
        else:
            self.label.setPixmap(self.sprites[self.current_frame])
            self.label.update()

    def unpack_sprites(self, filename):
        input_pixmap = QPixmap(filename)
        frame_count = input_pixmap.width() // ITEM_SIZE.width()
        sprites = []

        for current_frame in range(1, frame_count + 1):
            sprites.append(input_pixmap.copy(current_frame * ITEM_SIZE.width(),
                                             0, ITEM_SIZE.width(),
                                             ITEM_SIZE.height()))

        return sprites


class GameField(QGraphicsView):
    PIXMAPS = {}
    SPRITES = {}

    def __init__(self, parent_window, game, screen_size):
        super().__init__(parent_window)
        self.max_map_size = min(screen_size.width() // ITEM_SIZE.width(),
                                screen_size.height() // ITEM_SIZE.height())

        self.game_map = game.game_map
        self.game = game
        self.bonusTimer = QTimer()
        self.map_painter = QPainter()

        self.scene = QGraphicsScene()
        self.init_default_state()

        GameField.PIXMAPS = self.load_pixmaps()
        GameField.SPRITES = self.load_sprites()

        self.init_map(self.game.levels[self.game.current_level])
        self.pacman_animation = PacmanGrapghic(self, self.game.pacman)
        self.ghosts_animation = self.init_ghosts_animation()

        self.show()
        self.stop_units_motion()
        QTimer.singleShot(2000, self.continue_units_motion)

    def init_ghosts_animation(self):
        ghosts_animation = []
        ghost_size = QSize(26, 26)
        for ghost in self.game.ghosts:
            ghosts_animation.append(GhostGraphic(self, ghost, ghost_size))
        return ghosts_animation

    def init_default_state(self):
        self.scene.setBackgroundBrush(Qt.black)
        self.scene.setSceneRect(0, 0, 608, 608)
        self.setFixedSize(608, 608)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.bonusTimer.timeout.connect(self.generate_extra_food)
        self.bonusTimer.start(30000)
        self.setScene(self.scene)

    def generate_extra_food(self):
        free_space_items = self.get_free_space_items()
        special_food = ["cherry", "heart", "shield"]
        if len(free_space_items) != 0:
            random_index = int(random.uniform(0, len(free_space_items)))
            random_food_index = int(random.uniform(0, 3))
            extra_food = special_food[random_food_index]

            item = free_space_items[random_index]
            pos_x = item.x // ITEM_SIZE.width()
            pos_y = item.y // ITEM_SIZE.height()
            item.pixmap = QPixmap(self.PIXMAPS[extra_food])
            if self.game.game_map[pos_y][pos_x] == MapItem.food:
                self.game.food_count -= 1

            self.game.game_map[pos_y][pos_x] = extra_food
            self.scene.update()

    def get_free_space_items(self):
        result = []
        game_map = self.game.game_map
        for item in self.scene.items():
            pos_x = item.x // ITEM_SIZE.width()
            pos_y = item.y // ITEM_SIZE.height()
            if game_map[pos_y][pos_x] == 0:
                result.append(item)
        return result

    def init_map(self, file_name):
        with open(file_name) as map_file:
            for (i, cells) in enumerate(map_file):
                for (j, cell) in enumerate(cells):
                    if j > self.max_map_size - 1:
                        raise Exception("too large size of the game_map")
                    if cell in self.PIXMAPS.keys():
                        self.scene.addItem(SceneItem(self.PIXMAPS[cell],
                                                     j, i, ITEM_SIZE))

    def build_ghost_house(self, x, y):
        for i in range(5):
            current_pixmap = QPixmap("ghostHouse{}.png".format(i + 1))
            self.scene.addItem(SceneItem(current_pixmap, x + i, y, ITEM_SIZE))

    def keyPressEvent(self, key_event):
        if key_event.key() in directions:
            self.game.pacman.next_direction = key_event.key()

        if key_event.key() == 82:
            self.record_table = RecordTable(self)

        if key_event.key() == 80:
            self.stop_units_motion()

        if key_event.key() == 16777220:
            self.continue_units_motion()

    def stop_units_motion(self):
        self.pacman_animation.motion_timer.stop()
        for ghost_animation in self.ghosts_animation:
            ghost_animation.motion_timer.stop()

    def continue_units_motion(self):
        self.pacman_animation.motion_timer.start(self.game.pacman.speed)
        for ghost_animation in self.ghosts_animation:
            ghost_animation.motion_timer.start(ghost_animation.ghost.speed)

    def show_units_on_start_postion(self):
        self.pacman_animation.init_start_position()
        for ghost_animation in self.ghosts_animation:
            ghost_animation.init_start_position()

    def load_pixmaps(self):
        images_folder_path = os.path.join(os.path.curdir, "Images")
        default_pixmaps = self.init_default_pixmaps()
        pixmaps = {}

        for name, key in MAP_ITEMS_PATHS.items():
            img_path = os.path.join(images_folder_path, name)
            if os.path.exists(img_path):
                pixmaps[key] = QPixmap(img_path)
            else:
                if name in default_pixmaps.keys():
                    pixmaps[name] = default_pixmaps[name]
                    print("WARNING: image file {0} doesn't exist. This image "
                          "was replaced by default value".format(name))
        return pixmaps

    def load_sprites(self):
        images_folder_path = os.path.join(os.path.curdir, "Images")
        sprites = {}
        for name in SPRITES_PATH:
            img_path = os.path.join(images_folder_path, name)
            if os.path.exists(img_path):
                sprites[name] = QPixmap(img_path)
            else:
                raise FileNotFoundError("""ERROR: file {0} for sprite
                                        animation doesn't exist.
                                        Please try to recover this file
                                        in folder Images""".format(name))
        return sprites

    def init_default_pixmaps(self):
        default_pixmaps = {}
        for name, color in [("food.png", Qt.white), ("wall.png", Qt.darkBlue),
                            ("empty.png", Qt.black),
                            ("cherry.png", Qt.darkRed),
                            ("heart.png", Qt.red),
                            ("shield.png", Qt.gray)]:
            default_pixmaps[name] = QPixmap(32, 32)
            default_pixmaps[name].fill(color)

        return default_pixmaps

    def show_game_over_dialog(self, title):
        dialog = GameOverDialog(self.game, title, self)
        result = dialog.exec_()
        user_name = dialog.textValue()
        user_scores = self.game.pacman.scores

        if user_name != "":
            self.game.update_records(user_name, user_scores)

        if result == QDialog.Accepted:
            dialog.close()
            self.record_table = RecordTable(self, user_name, user_scores)
        else:
            self.restart_game()

    def restart_game(self):
        if self.game.current_level > self.game.get_number_levels():
            self.game.current_level = 0

        self.game.restart()

        self.scene.clear()
        self.init_map(self.game.levels[self.game.current_level])
        self.update()
        self.show_units_on_start_postion()
        self.stop_units_motion()
        QTimer.singleShot(2000, self.continue_units_motion)

    def next_level(self):
        self.game.current_level += 1
        if self.game.current_level > self.game.get_number_levels() - 1:
            self.game.current_level = 0

        level_file_name = self.game.levels[self.game.current_level]
        self.game.go_to_next_level()
        self.scene.clear()
        self.init_map(level_file_name)
        self.update()
        self.show_units_on_start_postion()
        self.stop_units_motion()
        QTimer.singleShot(2000, self.continue_units_motion)


class PacmanGrapghic(QLabel):
    def __init__(self, game_field, pacman):
        super().__init__(game_field)
        self.animation = UnitAnimation(pacman.name + '.png', ITEM_SIZE, self)
        self.death_animation = PacmanDeathAnimation("PacmanDeath.png", self)
        self.motion_timer = QTimer()
        self.pacman = pacman

        self.init_start_state()
        self.setAlignment(Qt.AlignCenter)
        self.setFixedSize(ITEM_SIZE)
        self.init_start_position()

    def init_start_position(self):
        self.animation.current_direction = Direction.left
        self.animation.current_frame = 0
        self.animation.next_frame()
        self.move(self.pacman.start_x * self.width(),
                  self.pacman.start_y * self.width())

    def init_start_state(self):
        self.move(self.pacman.x * self.width(), self.pacman.y * self.height())
        self.setPixmap(self.animation.get_current_pixmap())
        self.motion_timer.timeout.connect(self.act)
        self.motion_timer.start(self.pacman.speed)

    def move_unit(self):
        game_field = self.parentWidget()
        self.pacman.move()

        for ghost in game_field.game.ghosts:
            ghost.check_pacman_death()

        x_pos = self.pacman.x * self.width()
        y_pos = self.pacman.y * self.height()
        pacman_direction = self.pacman.current_direction

        self.animation.current_direction = pacman_direction
        self.move(x_pos, y_pos)

        food = game_field.scene.itemAt(x_pos, y_pos, QTransform())
        if food is not None:
            game_field.scene.removeItem(food)

    def processed_death(self):
        game_field = self.parent()
        ghosts = game_field.game.ghosts
        for ghost in ghosts:
            ghost.init_start_state()

        self.animation.timer.stop()
        self.death_animation.timer.start(70)
        self.clear()

        number_levels = game_field.game.get_number_levels()

        if self.pacman.lifes < 0:
            game_field.show_game_over_dialog("You lose")

        if game_field.game.current_level > number_levels:
            game_field.show_game_over_dialog("Congratulations! You won!")

    def act(self):
        game_field = self.parent()
        if game_field.game.food_count == 0:
            game_field.next_level()

        if not self.animation.timer.isActive():
            if not self.death_animation.timer.isActive():
                self.clear()
                self.pacman.init_start_state()
                self.animation.timer.start(70)
        else:
            if self.pacman.is_dead:
                self.processed_death()
            else:
                self.move_unit()


class GhostGraphic(QLabel):
    def __init__(self, game_field, ghost, size):
        super().__init__(game_field)
        self.animation = UnitAnimation(ghost.name + '.png', size, self)
        self.motion_timer = QTimer()
        self.ghost = ghost
        self.init_start_state()
        self.setAlignment(Qt.AlignCenter)
        self.setFixedSize(ITEM_SIZE)
        self.animation.current_direction = self.ghost.direction
        self.init_start_position()

    def init_start_position(self):
        self.move(self.ghost.start_x * self.width(),
                  self.ghost.start_y * self.width())

    def init_start_state(self):
        self.move(self.ghost.x * self.width(), self.ghost.y * self.height())
        self.setPixmap(self.animation.get_current_pixmap())
        self.motion_timer.timeout.connect(self.move_ghost)
        self.motion_timer.start(self.ghost.speed)

    def move_ghost(self):
        self.ghost.move()
        x_pos = self.ghost.x * self.width()
        y_pos = self.ghost.y * self.height()

        self.animation.current_direction = self.ghost.direction
        self.move(x_pos, y_pos)


class SceneItem(QGraphicsItem):
    def __init__(self, pixmap, x, y, size):
        super().__init__()
        self.pixmap = pixmap
        self.size = size
        self.x = x * self.size.width()
        self.y = y * self.size.height()

    def boundingRect(self):
        return QRectF(self.x, self.y, self.size.width(), self.size.height())

    def paint(self, painter, style, widget=None):
        painter.drawPixmap(self.x, self.y, self.size.width(),
                           self.size.height(), self.pixmap)


class RecordTable(QWidget):
    def __init__(self, game_field, user_name="", user_scores=0):
        super().__init__()
        game_field.stop_units_motion()
        self.setFixedSize(200, 500)
        self.setWindowModality(0)
        self.game_field = game_field
        self.records_list = self.game_field.game.records
        self.setWindowTitle("Records")
        self.vertical_layout = QVBoxLayout()
        self.set_records_list(user_name, user_scores)
        self.setLayout(self.vertical_layout)
        self.show()

    def set_records_list(self, user_name, user_scores):
        palette = QPalette()
        for (i, record) in enumerate(self.records_list):
            current_record_label = QLabel("{0}. {1}: {2}".format(i + 1,
                                                                 record[0],
                                                                 record[1]))
            current_record_label.setFrameStyle(QFrame.Panel | QFrame.Raised)
            current_record_label.setAutoFillBackground(True)
            if record[0] == user_name and record[1] == user_scores:
                palette.setColor(QPalette.Background, Qt.green)
                current_record_label.setPalette(palette)

            if i == 15:
                palette.setColor(QPalette.Background, Qt.red)
                current_record_label.setPalette(palette)

            self.vertical_layout.addWidget(current_record_label)

    def closeEvent(self, close_event):
        if self.game_field.game.pacman.lifes < 0:
            QCoreApplication.instance().quit()
        else:
            self.game_field.continue_units_motion()


class GameOverDialog(QInputDialog):
    def __init__(self, game, title, parent=None):
        super().__init__(parent)
        self.widget = QWidget()
        self.setWindowTitle(title)
        self.setLabelText("Your result: {0}. ".format(game.pacman.scores) +
                          "Enter your name, high scorer!\n"
                          "push 'left ctrl' to see record tab")
        self.game = game
        self.record_table = None
        self.setOkButtonText("Exit")
        self.setCancelButtonText("Try again")
        self.setWindowModality(0)
        self.setModal(False)

        self.setOption(QInputDialog.UseListViewForComboBoxItems)
        self.show()



