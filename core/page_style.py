import os
import re
from collections import OrderedDict
from core.html_builder import LineElement
from core.exceptions import SourceFileExistsError


class PageStyle:
    def __init__(self, style_path):
        self.file_path = style_path
        self.theme = self._unpack(self.file_path)
        self.css_classes = self._get_css_classes()

    def _unpack(self, theme_path):
        if not os.path.exists(theme_path):
            _, tail = os.path.split(theme_path)
            raise SourceFileExistsError("css", tail)

        with open(self.file_path) as css_file:
            return css_file.read()

    def get_link(self):
        attributes = [("type", "text/css"), ("rel", "stylesheet"),
                      ("href", self.file_path)]

        return LineElement("link", OrderedDict(attributes))

    def _get_css_classes(self):
        return re.findall(r'\.(\w*)\{.*?\}', self.theme)
