class SourceFileExistsError(FileExistsError):
    def __init__(self, source_folder, file_name):
        self.source_folder = source_folder
        self.file_name = file_name

    def __str__(self):
        message = "File {} doesn't exist in folder \"{}\"!"
        return message.format(self.file_name, self.source_folder)


class UnexpectedSymbolError(RuntimeError):
    def __init__(self, symbol, line, column):
        self.symbol = symbol
        self.line = line
        self.column = column

    def __str__(self):
        message = "symbol: {}, line {}, column {}"
        return message.format(self.symbol, self.line, self.column)
