from core.lexical_analyser import Lexeme
from core.html_builder import LineElement, Table, HTMLFileBuilder
from core.lexical_analyser import LexicalAnalyzer, choose_lang
from core.page_style import PageStyle
import os.path


class Translator:
    def __init__(self, style_path, lang_folder_path):
        self._page_style = PageStyle(style_path)
        self.lang_folder_path = lang_folder_path

    def translate(self, source_path, target_path):
        if os.path.isdir(source_path):
            raise FileNotFoundError("target path must "
                                    "be a path to file")
        else:
            self._translate_file(source_path, target_path)

    def _translate_file(self, source_path, target_path):
        lang_description = choose_lang(source_path, self.lang_folder_path)
        lex_analyzer = LexicalAnalyzer(lang_description)

        with open(source_path, "r", encoding="utf-8") as source_file:
            style = self._page_style.get_link()
            html_builder = HTMLFileBuilder(source_file.name, style)
            lexemes = lex_analyzer.analyze(source_file)

            table = self._build_code_table(self.highlight(lexemes))
            html_builder.body.add(table)
            html_builder.write_to_file(target_path)

    def _build_code_table(self, highlighted_lexemes):
        line_content = ""
        table = Table()
        for token in highlighted_lexemes:
            if token == "\n":
                self.append_line(table, line_content)
                line_content = ""
            else:
                line_content += token
        self.append_line(table, line_content)
        return table

    def append_line(self, table, line_content):
        table.add_row()
        row_number = table.get_row_number() - 1
        table.add_cell({"class": "line_number"}, row_number + 1, row_number)
        table.add_cell({"class": "code_line"}, line_content, row_number)

    def highlight(self, lexemes):
        for lexeme in lexemes:
            if self.is_multiline(lexeme):
                parts = self.split_multiline_lexeme(lexeme)
                for lex_part in parts[:-1]:
                    yield self._paint(lex_part)
                    yield '\n'
                yield self._paint(parts[-1])
            else:
                yield self._paint(lexeme)

    def is_multiline(self, lexeme):
        return len(lexeme.value) > 1 and '\n' in lexeme.value

    def split_multiline_lexeme(self, lexeme):
        parts = lexeme.value.split("\n")
        result = []
        for i, lex_part in enumerate(parts):
            result.append(Lexeme(lexeme.type, lex_part, lexeme.line + i, 0))
        return result

    def _paint(self, lexeme):
        if lexeme.type in self._page_style.css_classes:
            line_element = LineElement("span", {"class": lexeme.type},
                                       lexeme.value)
            return str(line_element)
        else:
            return lexeme.value
