class HTMLElement:
    def __init__(self, name, parent_element=None, attributes=None):
        self.parent = parent_element
        self.name = name
        self._attributes = attributes

    @property
    def parent(self):
        return self._parent

    @parent.setter
    def parent(self, parent_element):
        if isinstance(parent_element, HTMLElement) or parent_element is None:
            self._parent = parent_element
        else:
            raise ValueError("Parent element must be HTMLElement or None")

    @property
    def attributes(self):
        return self._attributes

    @attributes.setter
    def attributes(self, attributes_dict):
        if isinstance(attributes_dict, dict) or attributes_dict is None:
            self._attributes = attributes_dict
        else:
            raise ValueError("Attributes must be dict or None")

    def delete_attributes(self):
        self._attributes = None

    def _transform_attributes_to_string(self):
        result = ""
        if self._attributes is not None:
            for attr, value in self._attributes.items():
                result += ' {}="{}"'.format(attr, value)
        return result

    def _get_open_tag(self):
        attr = self._transform_attributes_to_string()
        return self.name if attr == "" else "{}{}".format(self.name, attr)

    def get_nesting_level(self):
        if self._parent is not None:
            return self.parent.get_nesting_level() + 1
        return 0


class BlockElement(HTMLElement):
    def __init__(self, name, parent_element=None):
        super().__init__(name, parent_element)
        self._content = []

    def add(self, element):
        if isinstance(element, HTMLElement):
            self._content.append(element)
            element.parent = self
        else:
            raise ValueError("Value must be HTMLElement")

    def delete_content(self):
        self._content.clear()

    def __str__(self):
        indent = "\t" * self.get_nesting_level()
        content = ""
        open_tag = self._get_open_tag()

        for element in self._content:
            content += "\n{}".format(element)

        return "{0}<{1}>{2}\n{0}</{3}>\n".format(indent, open_tag,
                                                 content, self.name)


class LineElement(HTMLElement):
    def __init__(self, name, attributes=None, content=""):
        super().__init__(name, None, attributes)
        self.content = content

    def __str__(self):
        indent = "\t" * self.get_nesting_level()
        open_tag = self._get_open_tag()

        if self.content == "":
            return "{}<{}>".format(indent, open_tag)

        return "{}<{}>{}</{}>".format(indent, open_tag,
                                      self.content, self.name)


class Table(BlockElement):
    def __init__(self, parent_element=None):
        super().__init__("table", parent_element)

    def get_row_number(self):
        return len(self._content)

    def add_row(self):
        new_row = BlockElement("tr", self)
        self.add(new_row)

    def add_cell(self, attributes, content, row_number):
        if row_number > len(self._content):
            raise IndexError("row doesn't exist")

        new_cell = LineElement("td", attributes, content)
        self._content[row_number].add(new_cell)


class HTMLFileBuilder:
    def __init__(self, title, style):
        self.html_block = BlockElement("html")
        self.head = BlockElement("head")
        self.body = BlockElement("body")

        self.html_block.add(self.head)
        self.html_block.add(self.body)

        self.style = style
        self._init_start_info(title)

    def _init_start_info(self, title):
        self.head.add(LineElement("meta", {"charset": "utf-8"}))
        self.head.add(LineElement("title", content=title))
        self.head.add(self.style)

    def write_to_file(self, file_path):
        with open(file_path, "w", encoding="utf-8") as html_file:
            html_file.write("<!doctype html>\n")
            content = str(self.html_block)
            html_file.write(content)
