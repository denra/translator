import re
import os
import yaml
from collections import namedtuple, OrderedDict
from core.exceptions import UnexpectedSymbolError

Lexeme = namedtuple("Lexeme", ["type", "value", "line", "column"])


def choose_lang(source_path, lang_folder):
    source_file_extension = os.path.splitext(source_path)[1]
    if source_file_extension == "":
        raise NameError("file must have extension")

    for lang_path in os.listdir(lang_folder):
        if lang_path.endswith(".yaml"):
            full_path = os.path.join(lang_folder, lang_path)
            with open(full_path) as lang_file:
                content = yaml.load(lang_file)
                extensions = content["config"]["fileExtensions"]
                if source_file_extension[1:] in extensions:
                    return content["langDescription"]

    error_message = "Can't find a language description for " \
                    "file with {} extension ".format(source_file_extension)
    raise FileNotFoundError(error_message)


class LexicalAnalyzer:
    def __init__(self, lang_description):
        self.lex_description = self.get_lexemes_description(lang_description)
        self.last_lexeme = None
        self.column = 0
        self.line = 1
        self.next_char = " "

    def get_lexemes_description(self, lang_description):
        lex_definition = OrderedDict()
        for lexeme in lang_description:
            for name, value in lexeme.items():
                if isinstance(value, str):
                    expr = re.compile(value)
                    lex_definition[name] = expr
                else:
                    for lex_name, expr in self.get_composite_lex(name, value):
                        lex_definition[lex_name] = expr
        return lex_definition

    def get_composite_lex(self, name, content):
        for element in content:
            for key, value in element.items():
                lex_name = "{}_{}".format(name, key)
                expr = re.compile(value)
                yield (lex_name, expr)

    def analyze(self, source_file):
        current_str = ""
        while self.next_char != '':
            recognized_lexeme = self._recognize_lexeme(current_str)
            if current_str == "" or recognized_lexeme is not None:
                current_str += self.get_next_char(source_file)
                self.last_lexeme = recognized_lexeme
            else:
                if self.is_composite_lexeme(self.last_lexeme, "start"):
                    yield self.get_composite_lexeme(source_file, current_str)
                    current_str = ""
                else:
                    yield self.last_lexeme
                    current_str = current_str[-1]
        yield self._recognize_lexeme(current_str)

    def _recognize_lexeme(self, input_str):
        for lex_type, lexeme in self.lex_description.items():
            match_obj = lexeme.fullmatch(input_str)
            if match_obj is not None:
                return Lexeme(lex_type, input_str, self.line,
                              self.column - len(input_str))

        if len(input_str) == 1:
            raise UnexpectedSymbolError(input_str, self.line, self.column)

        return None

    def get_next_char(self, source_file):
        if self.next_char == '\n':
            self.line += 1
            self.column = 0
        self.next_char = source_file.read(1)
        if self.next_char != "":
            self.column += 1
        return self.next_char

    def is_composite_lexeme(self, lexeme, part_name):
        result = re.match(r'.*?_{}\d*'.format(part_name), lexeme.type)
        return result is not None

    def get_composite_lexeme(self, source_file, lex_start):
        lex_type = self.last_lexeme.type.partition("_")[0]
        lex_end = self.last_lexeme.type.replace("_start", "_end")
        content = lex_start
        while not self.is_end_of_composite_lexeme(lex_end, content):
            content += self.get_next_char(source_file)

        return Lexeme(lex_type, content,
                      self.last_lexeme.line, self.last_lexeme.column)

    def is_end_of_composite_lexeme(self, end_type, content):
        res = self.lex_description[end_type].search(content, 1)
        return res is not None or self.next_char == ''
