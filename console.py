import sys
import argparse
from core.translator import Translator
import os

__version__ = '1.0'
__author__ = 'Razbitsky Denis'
__email__ = 'denrazv@mail.ru'

if sys.version_info < (3, 4):
    print('Use python >= 3.4')
    sys.exit()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--version', help="show program version",
                        action='version', version=__version__)
    parser.add_argument("style", help="style of html page")
    parser.add_argument("source_path",
                        help="path of source file or source directory")
    parser.add_argument("result_folder",
                        help="path of translated file or directory")

    return parser.parse_args()


def main():
    args = parse_args()
    style_path = os.path.join(os.curdir, "data", "css", args.style + ".css")
    lang_folder_path = os.path.join(os.curdir, "data", "lang")

    translator = Translator(os.path.abspath(style_path),
                            os.path.abspath(lang_folder_path))
    translator.translate(args.source_path, args.result_folder)

if __name__ == '__main__':
    main()
